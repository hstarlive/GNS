#ifndef _GNS_MALLOC_H
#define _GNS_MALLOC_H 110

#ifdef __cplusplus
extern "C" {
#endif


#include "stdlib.h"
#include "string.h"
#include "stdint.h"



#define BUFFER_INITIALIZED     0x01 //缓存初始化标志位
#define BUFFER_MALLOC          0x02 

/*自定义动态内存分配 */
#ifndef LIB_ALLOC_FUNC
#define LIB_ALLOC_FUNC         malloc
#endif
#ifndef LIB_FREE_FUNC
#define LIB_FREE_FUNC          free
#endif

#ifndef BUFFER_FAST
#define BUFFER_FAST            1
#endif

typedef struct _BUFFER_t {
	uint32_t Size;           
	uint32_t In;             
	uint32_t Out;            
	uint8_t* Buffer;         
	uint8_t Flags;           /
	uint8_t StringDelimiter; 
	void* UserParameters;    
} BUFFER_t;


uint8_t BUFFER_Init(BUFFER_t* Buffer, uint32_t Size, void* BufferPtr);
void BUFFER_Free(BUFFER_t* Buffer);
uint32_t BUFFER_Write(BUFFER_t* Buffer, const void* Data, uint32_t count);
uint32_t BUFFER_WriteToTop(BUFFER_t* Buffer, const void* Data, uint32_t count);
uint32_t BUFFER_Read(BUFFER_t* Buffer, void* Data, uint32_t count);
uint32_t BUFFER_GetFree(BUFFER_t* Buffer);
uint32_t BUFFER_GetFull(BUFFER_t* Buffer);
uint32_t BUFFER_GetFullFast(BUFFER_t* Buffer);
void BUFFER_Reset(BUFFER_t* Buffer);
int32_t BUFFER_FindElement(BUFFER_t* Buffer, uint8_t Element);
int32_t BUFFER_Find(BUFFER_t* Buffer, const void* Data, uint32_t Size);

#define BUFFER_SetStringDelimiter(Buffer, StrDel)  ((Buffer)->StringDelimiter = (StrDel))

uint32_t BUFFER_WriteString(BUFFER_t* Buffer, const char* buff);
uint32_t BUFFER_ReadString(BUFFER_t* Buffer, char* buff, uint32_t buffsize);
int8_t BUFFER_CheckElement(BUFFER_t* Buffer, uint32_t pos, uint8_t* element);

#ifdef __cplusplus
}
#endif

#endif
