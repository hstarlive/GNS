#ifndef _GNS_H
#define _GNS_H 


#ifdef __cplusplus
extern "C" {
#endif


#include "stdlib.h"
#include "string.h"
#include "stdint.h"
#include "stdio.h"
#include "math.h"


#include "gns_malloc.h"


#include "gns_cfg.h"



//返回结果
typedef enum _GNS_Result_t {
    gpsOK,                              
    gpsERROR,                           
    gpsNODATA,                          
    gpsOLDDATA,                         
    gpsNEWDATA                          
} GNS_Result_t;


typedef enum _GNS_Fix_t {
    GNS_Fix_Invalid = 0x00,            
    GNS_Fix_GNS = 0x01,                
    GNS_Fix_DGNS = 0x02                
} GNS_Fix_t;


typedef enum GNS_FixMode_t {
    GNS_FixMode_Invalid = 0x01,         
    GNS_FixMode_2D = 0x02,              
    GNS_FixMode_3D = 0x03               
} GNS_FixMode_t;

//卫星信息
typedef struct _GNS_Sat_t {
    uint8_t ID;                        
    uint8_t Elevation;                 
    uint16_t Azimuth;                  
    uint8_t SNR;                       
} GNS_Sat_t;

//时间
typedef struct _GNS_Time_t {
    uint8_t Hours;                      
    uint8_t Minutes;                    
    uint8_t Seconds;                    
    uint8_t Hundreds;                   
    uint16_t Thousands;                 
} GNS_Time_t;

//日期
typedef struct _GNS_Date_t {
    uint8_t Day;                        
    uint8_t Month;                      
    uint16_t Year;                      
} GNS_Date_t;

//速度
typedef enum _GNS_Speed_t {
    /* 公制 */
    GNS_Speed_KilometerPerSecond,      
    GNS_Speed_MeterPerSecond,           
    GNS_Speed_KilometerPerHour,        
    GNS_Speed_MeterPerMinute,          
    /* 英制 */
    GNS_Speed_MilePerSecond,           
    GNS_Speed_MilePerHour,              
    GNS_Speed_FootPerSecond,            
    GNS_Speed_FootPerMinute,           
    /* 步*/
    GNS_Speed_MinutePerKilometer,       
    GNS_Speed_SecondPerKilometer,      
    GNS_Speed_SecondPer100Meters,       
    GNS_Speed_MinutePerMile,            
    GNS_Speed_SecondPerMile,           
    GNS_Speed_SecondPer100Yards,        
    /* 海里 */
    GNS_Speed_SeaMilePerHour,           
} GNS_Speed_t;

//距离和方位
typedef struct _GNS_Distance_t  {
    float LatitudeStart;               
    float LongitudeStart;               
    float LatitudeEnd;                  
    float LongitudeEnd;                 
    float Distance;                     
    float Bearing;                      
} GNS_Distance_t;

//数据类型
typedef enum _GNS_CustomType_t {
    GNS_CustomType_Float,				
    GNS_CustomType_Int,					
    GNS_CustomType_String,				
	GNS_CustomType_Char,				
	GNS_CustomType_LatLong				
} GNS_CustomType_t;

//自定义解析内容结构体
typedef struct _GNS_Custom_t {
    const char* Statement;              
    uint8_t TermNumber;                 
    union {
        char S[13];                     
        char C;                        
        float F;                        
        float L;                        
        int I;                          
    } Value;                            
    GNS_CustomType_t Type;              
    uint8_t Updated;                    
} GNS_Custom_t;

//详见https://baike.baidu.com/item/GNS%E5%8D%8F%E8%AE%AE/306564
typedef struct _GNS_t {
    /* GNGGA  */
    float Latitude;                     
    float Longitude;                   
    float Altitude;                    
    GNS_Fix_t Fix;                     
    uint8_t SatsInUse;                  
    GNS_Time_t Time;                    

    /* GNGSA */
    GNS_FixMode_t FixMode;              
    uint8_t SatelliteIDs[12];           
    float HDOP;                         
    float PDOP;                         
    float VDOP;                         

    /* GNGSV */
    uint8_t SatsInView;                 
    GNS_Sat_t SatsDesc[24];            

    /* GNRMC */
    GNS_Date_t Date;                    
    uint8_t Valid;                      
    float Speed;                       
    float Coarse;                       
    float Variation;                       
    GNS_Custom_t* CustomStatements[GNS_CUSTOM_COUNT];   
    uint8_t CustomStatementsCount;      
} GNS_t;



//公共函数
GNS_Result_t GNS_Init(GNS_t* GNS);
uint32_t GNS_DataReceived(uint8_t* ch, size_t count);
GNS_Result_t GNS_Update(GNS_t* GNS);
float GNS_ConvertSpeed(float SpeedInKnots, GNS_Speed_t toSpeed);
GNS_Result_t GNS_DistanceBetween(GNS_Distance_t* Distance);
GNS_Result_t GNS_Custom_Add(GNS_t* GNS, GNS_Custom_t* Custom, const char* GPG_Statement, uint8_t TermNumber, GNS_CustomType_t Type);
GNS_Result_t GNS_Custom_Delete(GNS_t* GNS, GNS_Custom_t* Custom);




#ifdef __cplusplus
}
#endif

#endif
